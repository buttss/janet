from .JanetProcessor import JanetProcessor, entry_point
from events.Message import Message
from events.Hello import Hello
import time
import importlib
import requests
import gevent
import os

class Time(JanetProcessor):
    @entry_point
    def process(self):
        if type(self.event) == Message:
            if self.event.text == "?time":
                self.reply(time.time())

class Hi(JanetProcessor):
    @entry_point
    def process(self):
        if type(self.event) == Hello:
            self.send_message(self.version)

class Echo(JanetProcessor):
    @entry_point
    def process(self):
        if type(self.event) == Message:
            text = self.event.text
            if text.startswith("?echo"):
                message = text.partition(' ')[2]
                self.reply(message)

class Sleep(JanetProcessor):
    @entry_point
    def process(self):
        if type(self.event) == Message:
            text = self.event.text
            if text.startswith("?sleep"):
                duration = text.partition(' ')[2]
                try:
                    duration = float(duration)
                except ValueError:
                    self.reply(self.random_polite_refusal)
                    return
                self.reply(f"Putting worker to sleep for {duration}sec")
                gevent.sleep(duration)
                self.reply("Waking up worker")

class Ping(JanetProcessor):
    @entry_point
    def process(self):
        if type(self.event) == Message:
            text = self.event.text
            if "?ping" in text:
                self.reply("pong")

class Host(JanetProcessor):
    @entry_point
    def process(self):
        if type(self.event) == Message:
            text = self.event.text
            if text.startswith("?host"):
                self.reply(os.uname()[1])

class Version(JanetProcessor):
    @entry_point
    def process(self):
        if type(self.event) == Message:
            text = self.event.text
            if text.startswith("?vers"):
                self.reply(self.version)

class Processors(JanetProcessor):
    @entry_point
    def process(self):
        if type(self.event) == Message:
            text = self.event.text
            if text.startswith("?processors"):
                processors = importlib.import_module("JanetProcessors")
                for processor in processors:
                    self.reply(processor)


class Curl(JanetProcessor):
    @property
    def request_url(self):
        if len(self.split_text) > 1:
            return self.split_text[1]

    #@no_bots
    @entry_point
    def process(self):
        if type(self.event) == Message:
            text = self.event.text
            if text.startswith("?curl"):
                self.reply(requests.get(self.request_url))
