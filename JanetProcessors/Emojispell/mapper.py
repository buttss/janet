from random import shuffle
from JanetNeuron import JanetNeuron

class Mapper(JanetNeuron):
    text_to_translate = None
    mapping = {
            'a': [':mta-a:', ':a:', ':alm:', ':alme:', ':amazon:'],
            'b': [':mta-b:', ':b:', ':beam:'],
            'c': [':mta-c:', ':c:', ':pacman:'],
            'd': [':mta-d:'],
            'e': [':mta-e:', ':etsycloud:', ':etsy:', ':ie:'],
            'f': [':mta-f:', ':f:'],
            'g': [':mta-g:', ':google:'],
            'h': [':honda:', ':hyundai:', ':her:'],
            'i': [':information_source:', ':mens:'],
            'j': [':mta-j:'],
            'k': [':scrabble_k:', ':k:'],
            'l': [':mta-l:'],
            'm': [':mta-m:', ':m:', ':mcdonalds:'],
            'n': [':mta-n:'],
            'o': [':o:', ':o2:', ':bomb:', ':oracle:'],
            'p': [':pinterest:', ':paypal:'],
            'q': [':mta-q:', ':quora:'],
            'r': [':mta-r:', ':u_rochester_letter:'],
            's': [':mta-s:', ':cash:'],
            't': [':t:', ':mbta:', ':mta-t:', ':tesla:'],
            'u': [':u:'],
            'v': [':mta-v:', ':vim:', ':vimeo:', ':badge:'],
            'w': [':mta-w:', ':wegmans:'],
            'x': [':heavy_multiplication_x:', ':x:'],
            'y': [':yahoo-classic:', '🅈'],
            'z': [':mta-z:', ':zendesk:'],
            ' ': [':spacer:'],
            '1': [':mta-1:', ':one:', ':vote1:', ':unread:'],
            '2': [':mta-2:', ':two:', ':vote2:'],
            '3': [':mta-3:', ':three:', ':vote3:'],
            '4': [':mta-4:', ':four:'],
            '5': [':mta-5:', ':five:', ':vote5:'],
            '6': [':mta-6:', ':six:'],
            '7': [':mta-7:', ':seven:'],
            '8': [':eight:', ':vote8:'],
            '9': [':nine:'],
            '0': [':zero:', ':00:'],
            '!': [':exclamation:'],
            '?': [':question:', ':vote-unknown:'],
            '.': [':hole:'],
            '-': [':heavy_minus_sign:'],
            '+': [':heavy_plus_sign:']
            }

    def __init__(self, text):
        self.text_to_translate = text

    @property
    def as_emoji(self):
        return "".join([self.map_letter(letter) for letter in self.text_to_translate])

    def __repr__(self):
        return self.as_emoji

    def map_letter(self, letter):
        letter = letter.lower()
        if letter in self.mapping:
            emoji = self.mapping[letter]
            shuffle(emoji)
            return emoji[0]
        else: 
            return letter
