from JanetProcessors.JanetProcessor import JanetProcessor, entry_point
from events.Message import Message
from .mapper import Mapper

from models.User import User

class Emojispell(JanetProcessor):
    emojispell_words = ("?emojispell", "?ransomspell", "?ransomcase")

    @entry_point
    def process(self):
        is_message = type(self.event) == Message
        if not is_message:
            return

        found_our_words = self.event.text.startswith(self.emojispell_words)
        if found_our_words:
            plaintext = " ".join(self.clean_args)
            trans_text_lol = Mapper(plaintext)

            self.reply(trans_text_lol)
            return trans_text_lol
