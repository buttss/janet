from JanetProcessors.JanetProcessor import JanetProcessor, entry_point
from events.Message import Message
import sys

class Learn(JanetProcessor):
    exit_triggers = ("?die", "?exit", "?restart", "?reboot")
    
    @entry_point
    def process(self):
        if isinstance(self.event, Message) and not self.event.sender.is_bot:
            if self.event.text.startswith(self.exit_triggers):
                self.reply("Ohhhh oh no no no please don't")
                sys.exit()
