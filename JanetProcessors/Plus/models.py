from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from ORM import central_engine
import datetime

Base = declarative_base()

class PlusTarget(Base):
    __tablename__ = 'plus_target'

    target = Column(String(255), primary_key=True)
    plus_count = Column(Integer(), default=200)
    target_is_user = Column(Boolean(), default=False)


    def __repr__(self):
        return f"{self.target}: {self.plus_count}"

Base.metadata.create_all(central_engine)
