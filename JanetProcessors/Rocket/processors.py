from JanetProcessors.JanetProcessor import JanetProcessor, entry_point
from events.Message import Message
import os, random

from models.User import User

class Rocket(JanetProcessor):
    rocket_words = ("?rocket")
    # it's just ./rockets but for ~python~
    rockets_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "./rockets")

    @entry_point
    def process(self):
        is_message = type(self.event) == Message
        if not is_message:
            return

        found_our_words = self.event.text.startswith(self.rocket_words)
        if found_our_words:
            rocket_file = os.path.join(self.rockets_path, random.choice(os.listdir(self.rockets_path)))
            rocket = open(rocket_file).read()

            self.reply(rocket)
            return rocket
