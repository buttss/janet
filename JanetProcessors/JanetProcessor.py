import inspect
import pprint
from JanetNeuron import JanetNeuron

# All this decorator does is add the JANET_ENTRY
# attribute to the function object. 
# This lets us loop over all the attributes 
# of the processor and find ones with a JANET_ENTRY
class entry_point():
    def __init__(self, func):
        self.JANET_ENTRY = True
        self.func = func

    def __call__(self, *args, **kwargs):
        return self.func(*args, *kwargs)

    def __repr__(self):
        return self.func.__qualname__

class JanetProcessor(JanetNeuron):
    def __init__(self, event):
        self.event = event

    def reply(self, message):
        self.event.reply(message)

    @property
    def split_text(self):
        if not hasattr(self.event, "text"): return ""
        return self.event.text.split(' ')

    @property
    def clean_args(self):
        # lol these aren't clean
        args_minus_cmd = self.split_text[1:]
        if args_minus_cmd:
            return args_minus_cmd

    def process(self):
        return
        self.send_message("hi there, I'm Janet")

    def check_is_entry_point(self, attrname):
        try:
            attr_class = getattr(self.__class__, attrname)
            attr_class.JANET_ENTRY
        except AttributeError:
            return False

        return True

    @property
    def entry_points(self):
        if not hasattr(self, "_entries"):
            self._entries = []

        if not self._entries:
            self._entries = [getattr(self, attr) for attr in dir(self) if self.check_is_entry_point(attr)]

        return self._entries
