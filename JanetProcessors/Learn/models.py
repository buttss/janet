from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, UnicodeText, Unicode
from ORM import central_engine
import datetime

Base = declarative_base()

class Learn(Base):
    __tablename__ = 'learn'

    text = Column(UnicodeText())
    target = Column(Unicode(255)) # The target is the second word, usually a userid
    created_date = Column(DateTime, default=datetime.datetime.utcnow)
    learned_by = Column(Unicode(255))
    pk = Column(Integer, primary_key=True)

    def __repr__(self):
        return self.text

Base.metadata.create_all(central_engine)
