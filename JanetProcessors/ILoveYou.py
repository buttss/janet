from JanetProcessors.JanetProcessor import JanetProcessor, entry_point
from events.Message import Message

class ILoveYou(JanetProcessor):
    @entry_point
    def process(self):
        if type(self.event) == Message and not self.event.sender.is_bot:
            if "I love you".upper() in self.event.text.upper():
                self.send_message("https://imgur.com/a/WGV9niN")

