import unittest
import pprint
import time
import re

from JanetNeuron import JanetNeuron
from JanetWorker import JanetWorker
import JanetProcessors
from JanetProcessors import JanetProcessor, UserTracker, Plus, Emojispell
from JanetProcessors.Emojispell import mapper
from events.Event import Event
from events.Message import Message
from events.ReactionAdded import ReactionAdded
from models.User import User

class NeuronTest(unittest.TestCase, JanetNeuron):
    def setUp(self):
        self.print = pprint.pprint

    def test_auth(self):
        pass

    def test_list_users(self):
        user_list = self.list_users()
        self.assertEqual(user_list[0]['id'], 'USLACKBOT')

    def test_send_message(self):
        response = self.send_message(self.random_cactus)
        self.assertTrue(response['ok'])

    def test_janet_info(self):
        janet_info = self.janet_info
        self.assertTrue(janet_info['ok'])

    def test_emoji_list(self):
        emoji_list = self.all_emoji
        self.assertIn('cactus1', emoji_list)

class WorkerTest(unittest.TestCase, JanetWorker):
    def test_work(self):
        pass

class EventsTest(unittest.TestCase):
    def setUp(self):
        pass
        #self.message_raw = {"type": 

    def test_GenericEvent(self):
        pass

class ProcessorTest(unittest.TestCase):
    def setUp(self):
        self.raw_line = {'text': ':cactus12:', 'username': 'Janet', 'bot_id': 'BDL31KDB8', 'type': 'message', 'subtype': 'bot_message', 'team': 'TBN6XSDK7', 'channel': 'CDJLMKC1Y', 'event_ts': '1541182409.404500', 'ts': '1541182409.404500'}
        self.event = Event(self.raw_line)

    # metaprogramming horseshit
    def test_iter_janet_processors(self):
        for janet in JanetProcessors:
            janet(self.event)

    def test_no_load_parent_JanetProcessors(self):
        for janet in JanetProcessors:
            janet = janet(self.event)
            janet.entry_points

class DatastoreTest(unittest.TestCase):
    def test_NeuronConnect(self):
        import JanetProcessors
        for janet in JanetProcessors:
            janet("")

class ReprTest(unittest.TestCase):
    def setUp(self):
        self.raw_line = {'channel': 'CDJLMKC1Y', 'client_msg_id': '3a3d8ebf-2901-4d8d-aa3f-5f526c2c82a6', 'event_ts': '1544307315.032600', 'team': 'TBN6XSDK7', 'text': 'test', 'ts': '1544307315.032600', 'type': 'message', 'user': 'UBP4ZPGT1'}
        self.event = Message(self.raw_line)

    def test_sender_chroma_hash(self):
        # You could totally automate this. 
        # Even if it's just a regex to make sure it's any 3 colors
        # You could check that two are different if you care that much
        print(self.event.sender.chroma_hash, end='', flush=True)

class UserTest(unittest.TestCase, JanetNeuron):
    all_users = None
    def setUp(self):
        self.user = self.all_users[1]
        self.uid = self.user['id']

    @classmethod
    def setUpClass(cls):
        # listing users is a heavy API event, try to avoid overdoing it
        cls.all_users = JanetNeuron().list_users()
        cls.raw_hello_event = {'type': 'hello'}
        tracker = UserTracker.UserTracker(cls.raw_hello_event)
        tracker.update_user_tracking()
        tracker.session.close()

    def test_user_from_good_source_obj(self):
        user = User.from_source_obj(self.user, create=True)
        self.assertEqual(self.user, user._user)
        self.assertEqual(self.user['id'], self.uid)

    def test_user_from_bad_source_obj(self):
        with self.assertRaises(ValueError):
            User.from_source_obj(self.raw_hello_event, create=True)

    def test_userid_to_display_name(self):
        acqd_name = self.userid_to_display_name(self.uid)
        self.assertEqual(acqd_name, self.user['profile']['display_name_normalized'].lower())

    def test_user_from_display_name(self):
        u_real = User.from_display_name('slackbot')
        self.assertIsNotNone(u_real)

        u_bogus = User.from_display_name("seriously it's a dick move to make a user with this display name, please don't do it.")
        self.assertIsNone(u_bogus)

class PlusTest(unittest.TestCase):
    # TODO
    # ?+
    # ?+ string
    # ?+ multiple words
    # ?+ @real_username
    # ?+ @fake_username
    # ?+ @janet
    def setUp(self):
        pass

    def test_plus_normal_word(self):
        normal_word_raw = {'channel': 'CDJLMKC1Y', 'client_msg_id': '3a3d8ebf-2901-4d8d-aa3f-5f526c2c82a6', 'event_ts': '1544307315.032600', 'team': 'TBN6XSDK7', 'text': '?+ potato', 'ts': '1544307315.032600', 'type': 'message', 'user': 'UBP4ZPGT1'}
        message = Message(normal_word_raw)
        plus_processor = Plus.Plus(message)
        plus_processor.process(plus_processor)

    def test_plus_at_uid(self):
        at_user_raw = {'channel': 'CDJLMKC1Y', 'client_msg_id': '3a3d8ebf-2901-4d8d-aa3f-5f526c2c82a6', 'event_ts': '1544307315.032600', 'team': 'TBN6XSDK7', 'text': '?+ <@USLACKBOT>', 'ts': '1544307315.032600', 'type': 'message', 'user': 'UBP4ZPGT1'}
        message = Message(at_user_raw)
        plus_processor = Plus.Plus(message)
        plus_processor.process(plus_processor)

    def test_plus_display_name(self):
        display_name_raw = {'channel': 'CDJLMKC1Y', 'client_msg_id': '3a3d8ebf-2901-4d8d-aa3f-5f526c2c82a6', 'event_ts': '1544307315.032600', 'team': 'TBN6XSDK7', 'text': '?+ slackbot', 'ts': '1544307315.032600', 'type': 'message', 'user': 'UBP4ZPGT1'}
        message = Message(display_name_raw)
        plus_processor = Plus.Plus(message)
        plus_processor.process(plus_processor)

    def test_plus_react(self):
        raw_plus_event = {'event_ts': '1544806609.007000', 'item': {'channel': 'CDJLMKC1Y', 'ts': '1544806256.006900', 'type': 'message'}, 'item_user': 'UDJFMR4KW', 'reaction': 'heavy_plus_sign', 'ts': '1544806609.007000', 'type': 'reaction_added', 'user': 'UBP4ZPGT1'}
        reaction_event = ReactionAdded(raw_plus_event)
        plus_processor = Plus.Plus(reaction_event)
        plus_processor.process(plus_processor)


class ReactionTest(unittest.TestCase, JanetNeuron):
    def setUp(self):
        raw_event = {'event_ts': '1544806609.007000', 'item': {'channel': 'CDJLMKC1Y', 'ts': '1544806256.006900', 'type': 'message'}, 'item_user': 'UDJFMR4KW', 'reaction': 'heart', 'ts': '1544806609.007000', 'type': 'reaction_added', 'user': 'UBP4ZPGT1'}
        raw_plus_event = {'event_ts': '1544806609.007000', 'item': {'channel': 'CDJLMKC1Y', 'ts': '1544806256.006900', 'type': 'message'}, 'item_user': 'UDJFMR4KW', 'reaction': 'heavy_plus_sign', 'ts': '1544806609.007000', 'type': 'reaction_added', 'user': 'UBP4ZPGT1'}

        get_plusses_raw_message = {'text': '?plusses <@UBP4ZPGT1>', 'username': 'Janet', 'bot_id': 'BDL31KDB8', 'type': 'message', 'subtype': 'bot_message', 'team': 'TBN6XSDK7', 'channel': 'CDJLMKC1Y', 'event_ts': '1541182409.404500', 'ts': '1541182409.404500'}

        self.reaction_event = ReactionAdded(raw_event)
        self.plus_reaction_event = ReactionAdded(raw_plus_event)
        self.get_plusses_message = Message(get_plusses_raw_message)

    def test_react_attrs(self):
        plus_processor = Plus.Plus(self.reaction_event)
        plus_processor.process(plus_processor)
        plus_processor.reply("test")

    def test_get_plusses(self):
        pass

class EmojispellTest(unittest.TestCase):
    emoji_regex = '(:[\w_-]*:?)'
    
    def setUp(self):
        self.good_text = "not a girl"
        self.good_mapper = Emojispell.Mapper(self.good_text)

        self.unmappable_text = chr(233)
        self.un_mapper = Emojispell.Mapper(self.unmappable_text)

        self.raw_line = {'channel': 'CDJLMKC1Y', 'client_msg_id': '3a3d8ebf-2901-4d8d-aa3f-5f526c2c82a6', 'event_ts': '1544307315.032600', 'team': 'TBN6XSDK7', 'text': f"{Emojispell.Emojispell.emojispell_words[0]} {self.good_text}", 'ts': '1544307315.032600', 'type': 'message', 'user': 'UBP4ZPGT1'}
        self.event = Message(self.raw_line)

    def testMapNotBroken(self):
        self.good_mapper.as_emoji

    def testGoodTranslate(self):
        emojispelled_text = self.good_mapper.as_emoji
        self.assertTrue(self._check_untranslated_letters(emojispelled_text))

    def testMissingLetter(self):
        emojispelled_text = self.un_mapper.as_emoji
        self.assertFalse(self._check_untranslated_letters(emojispelled_text))

    def testProcessor(self):
        processor = Emojispell.Emojispell(self.event)
        self.assertIsNotNone(processor.process(processor))

    def _check_untranslated_letters(self, emojispelled_text):
        # Really just verifies that everything we translated is an emoji that can be found in the mapping
        # More like double-checking your work than testing really

        split_by_emoji = re.split(self.emoji_regex, emojispelled_text)
        text_emoji_set = set([item for item in split_by_emoji if item])

        mapper_emoji_list = []
        for letter_emoji_list in Emojispell.Mapper.mapping.values():
            mapper_emoji_list += [emojus for emojus in letter_emoji_list]
        mapper_emoji_set = set(mapper_emoji_list)

        return mapper_emoji_set.issuperset(text_emoji_set)

    def testRepr(self):
        pass


if __name__ == '__main__':
    unittest.main()
