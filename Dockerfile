FROM python:3.7

RUN mkdir /janet
WORKDIR /janet

COPY requirements.txt /tmp/
RUN pip install -Ur /tmp/requirements.txt

COPY . .

CMD ["./run_janet.py"]
# HEALTHCHECK --interval=30m --retries=1 CMD ["./janet_health.py"]
