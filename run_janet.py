#!/usr/bin/env python
# I don't prefer using env but in this case 
# it makes it flexible to use with or without a virtualenv

### THE ORDER OF THESE ONES MATTER ###
######################################

import gevent.monkey
gevent.monkey.patch_all()

import settings

import sentry_sdk
sentry_sdk.init(environment=settings.SENTRY_ENV)

######################################
### ### ##### ## ##### #### ###### ###

import time
import gevent
import gevent.queue
import random

from JanetWorker import JanetWorker
from JanetNeuron import JanetNeuron, StaticJanetNeuron 


if __name__ == '__main__':
    listen_janet = StaticJanetNeuron()
    q = gevent.queue.JoinableQueue(maxsize=settings.worker_count)

    # Make a bunch of workers to handle incoming 
    # messages. Or whatever. Put a thing in the queue
    # and Janet will try to deal with it
    for i in range(settings.worker_count):
        j = JanetWorker(q)
        gevent.spawn(j)

    if listen_janet.client.rtm_connect():
        while listen_janet.client.server.connected is True:
            for line in listen_janet.client.rtm_read():
                backoff_timer = 0
                while True:
                    backoff_timer += 1
                    try:
                        q.put_nowait(line)
                        break
                    except gevent.queue.Full:
                        listen_janet.send_message("Sorry, all my workers are busy.")
                        listen_janet.send_message("So I'm going to ignore you all for a while")
                        listen_janet.send_message(f"Listener sleeping for {2**backoff_timer}sec")
                        gevent.sleep(2**backoff_timer)
            gevent.sleep(settings.mainloop_delay)
    else:
        print("Connection Failed")
