from events.Event import Event

class Hello(Event):
    expected_type = "hello"

    def __repr__(self):
        return "HelloEvent"
