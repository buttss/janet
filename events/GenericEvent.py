from events.Event import Event

# this is the "idk wtf this is or what to do with it" class
class GenericEvent(Event):
    def __repr__(self):
        return "GenericEvent. Original type: {}".format(self.type)
