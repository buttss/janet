import pprint
from JanetNeuron import JanetNeuron

# ABC for all events
class Event(JanetNeuron):
    expected_type = None
    _sender = None

    def reply(self, *args, **kwargs):
        raise notimplementederror

    def __init__(self, raw_line):
        if (self.expected_type) and (self.expected_type != raw_line['type']):
            raise ValueError("cannot create {} from {} type".format(self.expected_type, raw_line['type']))
        self.raw_line = raw_line
        self.type = raw_line['type']

    # if you don't define a repr you just 
    # get a pretty formatted version of the raw line
    def __repr__(self):
        return pprint.pformat(self.raw_line)

