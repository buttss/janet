import pprint
from datetime import datetime
import time
import colored
from x256 import x256

from events.Event import Event
from models.User import User

class Message(Event):
    expected_type = "message"

    def __init__(self, raw_line):
        super().__init__(raw_line)
        if "message" in raw_line:
            self.raw_line = raw_line["message"]
            self.original_line = raw_line # just in case we want it later, y'know?
        if "text" in self.raw_line:
            self.text = self.raw_line["text"]

    @property
    def channel_id(self):
        return self.raw_line["channel"]

    @property
    def raw_timestamp(self):
        return float(self.raw_line['ts'])

    @property
    def timestamp(self):
        return datetime.fromtimestamp(self.raw_timestamp)

    @property
    def sender(self):
        if not self._sender:
            self._sender = User.from_source_object(self, create=True)
        return self._sender

    def reply(self, message):
        self.send_message(message, self.channel_id)

    def __repr__(self):
        return f"{self.sender.chroma_hash}{self.timestamp.strftime('%H:%M')} - {self.sender.display_name}: {self.text}"
