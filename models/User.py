from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, UnicodeText, Unicode
from sqlalchemy.orm import relationship, validates
from ORM import central_engine, session_factory
import datetime
import re
import sys
from models.Sender import Sender
from JanetNeuron import JanetNeuron

Base = declarative_base()

# Sender inherits from Neuron so you've got all that functionality for free if you want it
class User(Base, Sender):
    __tablename__ = 'janet_user'

    uid = Column(String(255), primary_key=True)
    _display_name = Column(String(255), index=True) # has an underscore so the property wrapper can still work
    created_date = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.user_info_from_id(self.uid):
            raise ValueError(f"No user for {self.uid} found via slack API")

    @classmethod
    def from_source_object(cls, source_obj, create=False):
        if hasattr(source_obj, 'raw_line'):
            source_obj = source_obj.raw_line

        user = None
        if 'id' in source_obj: # from user object
            user = source_obj
            source_uid = source_obj['id']
        elif 'bot_id' in source_obj: # from bot message event
            try:
                source_uid = source_obj['user']
            except KeyError:
                source_uid = source_obj['bot_id']
        elif 'user' in source_obj: # from message event
            source_uid = source_obj['user']
        else:
            raise ValueError(f"Could not construct Sender from {source_obj}")
        user_instance = cls.from_uid(source_uid, create=create)
        user_instance.source_obj = source_obj
        if user:
            user_instance._user = user
        if '_profile' in source_obj:
            user_instance._display_name = source_obj['profile']['display_name_normalized'].lower()
            session = self.new_session()
            session.add(user_instance)
            session.close()
        return user_instance

    # just an alias so I don't have to remember if I shortened it or not
    from_source_obj = from_source_object

    
    # Returns a User instance or None. Should probably raise instead of None? idk
    # for now fail_on_create will raise an exception if you try to get a user obj
    # for a uid that doesn't already exist
    @classmethod
    def from_uid(cls, uid, create=False):
        uid = cls.normalize_uid(uid)

        session = cls.new_session()
        user = session.query(cls).filter(cls.uid == uid).first()
        if not user: # user doesn't already exist in db
            if create: # weird way to phrase it
                display_name = JanetNeuron().userid_to_display_name(uid)
                user = cls(uid=uid, _display_name=display_name)
                session.add(user)
                session.commit()
                session.close()
            if not create:
                return None;
                #raise ValueError(f"Could not find pre-existing user with UID {uid}\nPerhaps you wanted to set create=True?")
        return user


        # Hmmmmm
        # All the from_ methods need to make their own session
        # Could possibly make it a static method?

    @classmethod
    def from_display_name(cls, display_name):
        # from_display_name doesn't get a create kwarg 
        # because that's not enough information to make a user from
        display_name = display_name.lower()
        session = session_factory() 
        user = session.query(cls).filter(cls._display_name == display_name).first()
        return user

    @property
    def display_name(self):
        if not self._display_name:
            display_name = super().display_name
            self._display_name = display_name

        return self._display_name

    @validates('uid')
    def validate_uid_field(self, key, uid):
        uid = self.normalize_uid(uid)
        return uid

    @classmethod
    def normalize_uid(cls, uid):
        display_uid_pattern = r'<@([A-Za-z0-9]*?)>' 
        if re.match(display_uid_pattern, uid):
            uid = re.sub(display_uid_pattern, r"\1", uid)
        return uid.upper()

    @property 
    def at_name(self):
        return f"<@{self.uid.upper()}>"


    def __repr__(self):
        return f"{self.display_name} -> {self.uid} ({self.at_name})"


Base.metadata.create_all(central_engine)
