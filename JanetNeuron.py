import importlib
import subprocess
import settings
import gevent
import random
from slackclient import SlackClient
from sentry_sdk import capture_message

import ORM

class JanetNeuron():
    _session = False

    @property 
    def janet_info(self):
        capture_message('bot.info slack API hit')
        if not hasattr(self, "_janet_info"):
            self._janet_info = self.client.api_call("bots.info")
        return self._janet_info

    @property
    def tokens(self):
        importlib.reload(settings)
        return settings.slack_tokens

    @property 
    def bot_token(self):
        return self.tokens['bot']

    @property
    def user_token(self):
        return self.tokens['user']

    @property
    def bot_client(self):
        return SlackClient(self.bot_token)
    client = bot_client

    @property
    def user_client(self):
        return SlackClient(self.user_token)

    def leave_channel(self, channel_id):
        response = self.client.api_call(
                "channels.leave",
                channel=channel_id,
                )
        return response

    def send_message(self, message, channel="#the-bad-place"):
        response = self.client.api_call(
                "chat.postMessage",
                channel=channel,
                as_user=True,
                text=message
                )
        return response

    def send_image_from_file(self, filename, channel="#the-bad-place"):
        sc.api_call("chat.postMessage", channel=channel, attachments=attachments)
        return response

    def user_info_from_id(self, userid):
        capture_message('users.info slack API hit')
        response = self.client.api_call(
                "users.info",
                user=userid
                )
        if not 'user' in response:
            return None

        return response['user']

    def userid_to_display_name(self, uid):
        user_info = self.user_info_from_id(uid)
        return user_info['profile']['display_name_normalized'].lower()

    def list_users(self):
        capture_message('users.list slack API hit')

        for i in range(5):
            response = self.client.api_call( "users.list")
            if response['ok']:
                return response['members']
            else:
                gevent.sleep(2**i)
        raise

    @property
    def all_emoji(self):
        response = self.user_client.api_call("emoji.list")
        emoji = response['emoji']
        return emoji

    @property
    def random_cactus(self):
        return ":cactus{}:".format(random.randint(2,15))

    @property
    def random_polite_refusal(self):
        phrases = ["No, sorry"]
        random.shuffle(phrases)
        return phrases[0]

    @property
    def random_exclamation(self):
        phrases = ["Y-uh oh"]
        random.shuffle(phrases)
        return phrases[0]

    @classmethod
    def new_session(cls):
        return ORM.session_factory()

    @property
    def session(self): 
        return self.new_session()

    def merge_noload(self, *args, **kwargs):
        self.merge(*args, load=False, **kwargs)

    def merge(self, *args, session=None, load=True):
        if not session: 
            session = self.new_session()
        merged_objects = [session.merge(arg, load=load) for arg in args]
        return (merged_objects, session)

    @property
    def version(self):
        git_vers = subprocess.check_output(["git", "rev-parse",  "--short", "HEAD"]).strip()
        if not git_vers:
            return "unknown version"
        else: 
            return git_vers


class StaticJanetNeuron(JanetNeuron):
    _client = None

    @property
    def client(self):
        if not self._client:
            self._client = SlackClient(self.bot_token)
        return self._client
