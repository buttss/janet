# Janet
![Janet!](https://media.giphy.com/media/xUOxeT5ZpZVStUPxC0/giphy.gif)

## Setup

### Requirements
- Python3.6 

### Install
```
git clone this thing
python3.6 -m virtualenv venv
. venv/bin/activate
pip install -Ur requirements.txt
```

Modify `settings.py` to add your slack token

```
./run_janet.py
```

## Writing Processor Plugins
If you want to make Janet do something, check out the `BasicProcessors` file in `JanetProcessors`
This has some simple examples you can get started with.

If you really want to get off the ground faster just make a new python module inside of 
the JanetProcessors directory. This module needs a minimum of two things to work.

1. A class that inherits from JanetProcessor
2. A method on that class decorated with @entry_point

like so:
```
from .JanetProcessor import JanetProcessor, entry_point

class MyProcessor(JanetProcessor):
    @entry_point
    def process(self):
    	print("processing an event!")
	do_something_else(self.event)
```

This processor will print `processing an event!` to the console every time your Janet
Receives any event from the Slack RTM API. That's all it takes to get started. The 
event data is stored in Event objects directly on the processor.

## Support Tools

### Sentry
@ https://sentry.io/alex-karpinski/janet/
