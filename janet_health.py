#!/usr/bin/env python
# This script sends a ?ping message and waits for a pong.
# Just a basic health check

import time
import sys

import sentry_sdk
sentry_sdk.init(environment="test_suite")

max_check_attempts = 5

from JanetNeuron import JanetNeuron, StaticJanetNeuron 

if __name__ == '__main__':
    listen_janet = StaticJanetNeuron()

    if listen_janet.client.rtm_connect():
        print("preparing to ping...")
        listen_janet.send_message("?ping")
        print("Ping sent. Sleeping 10sec")
        line = True
        while line:
            for line in listen_janet.client.rtm_read():
                print("reading from RTM")
                print(f"read: {line}")
                if 'text' in line:
                    if line['text'] == 'pong':
                        print("healthy")
                        sys.exit(0)
                print("no pong in response, sleeping and retrying")
                time.sleep(1)
                print("retrying rtm_read")
    else:
        print("Connection Failed")

print("unhealthy")
sys.exit(1)
